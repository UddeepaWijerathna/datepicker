var max = new Date().getFullYear(),
    min = max - 100,
    select = document.getElementById('yearId');

for (var i = max; i>=min; i--){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}



function calculateAge() {

    var today = new Date();
    
    var yearDiff = today.getFullYear() - document.forms["myForm"]["Year"].value;

    var month = document.forms["myForm"]["Month"].value;

    var name = document.forms["myForm"]["name"].value;

    var monthsDiff = today.getMonth()-(month-1);
    var dayDiff = today.getDate() - document.forms["myForm"]["Day"].value;

    if(dayDiff<0){
        dayDiff +=30;
        monthsDiff--
        if(monthsDiff<0){
            monthsDiff +=12
            yearDiff--
            document.getElementById("result").innerHTML = name+ " is "+yearDiff+" years "+ monthsDiff +" months "+dayDiff+" days old";
        }else{
            document.getElementById("result").innerHTML = name+ " is "+yearDiff+" years "+ monthsDiff +" months "+dayDiff+" days old";
        }

    }
    else if(monthsDiff<0){
        monthsDiff +=12
        yearDiff--
        document.getElementById("result").innerHTML = name+ " is "+yearDiff+" years "+ monthsDiff +" months "+dayDiff+" days old";
    }
    else{
    document.getElementById("result").innerHTML = name+ " is "+yearDiff+" years "+ monthsDiff +" months "+dayDiff+" days old";
    }
    
    return true;
}

 

function validateForm() {
    var name = document.forms["myForm"]["name"].value;
    var year = document.forms["myForm"]["Year"].value;
    var month = document.forms["myForm"]["Month"].value;
    var day = document.forms["myForm"]["Day"].value;
    var letters = new RegExp("^[A-Z a-z]+$");
    var today = new Date();

    if(name == ""){
        document.getElementById("error").innerHTML = "Name must be filled out";
    }
    else if(!letters.test(name)){
        document.getElementById("error").innerHTML = "Name can only contain alphabet";
    }
    else if ((year == "" || year =="Y") && (month ==""||month =="M")  && (day == "" || day =="D")){
        document.getElementById("error").innerHTML = "Date of Birth must be filled out";
    }else if((today.getFullYear()==year) && (today.getMonth()<(month-1))){
        document.getElementById("error").innerHTML = "Future Date cannot be the Date of Birth";
    }else if((today.getFullYear()==year)&& (today.getMonth()==(month-1))&& (today.getDate()<day)){
        document.getElementById("error").innerHTML = "Future Date cannot be the Date of Birth";
    }
    else{
        calculateAge();
    }
    

  }

function monthEnable(){


        document.getElementById("monthId").disabled = false;
        
       
       
        var monthArray = new Array();
        monthArray[0] = "January";
        monthArray[1] = "February";
        monthArray[2] = "March";
        monthArray[3] = "April";
        monthArray[4] = "May";
        monthArray[5] = "June";
        monthArray[6] = "July";
        monthArray[7] = "August";
        monthArray[8] = "September";
        monthArray[9] = "October";
        monthArray[10] = "November";
        monthArray[11] = "December";
    

        for(m = 0; m <= 11; m++) {
            var optn = document.createElement('option');
            optn.text = monthArray[m];
            optn.value = (m+1);
    
            document.getElementById('monthId').options.add(optn);
        }
    
        

}

function dayEnable(){

    document.getElementById("dayId").disabled = false;  

    var minday = 1,
    maxday = minday + 30,
    select = document.getElementById('dayId');

    var month = document.getElementById('monthId').value;

    if(month == "1" || month == "3" ||month == "5" ||month == "7" ||month == "8" ||month == "10" ||month == "12" ){
        for (var i = minday; i<=maxday; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        }
    }else if(month == "2"){
        if(leapYear()){
            for (var i = minday; i<=29; i++){
                var opt = document.createElement('option');
                opt.value = i;
                opt.innerHTML = i;
                select.appendChild(opt);
            }
        }else{
            for (var i = minday; i<=28; i++){
                var opt = document.createElement('option');
                opt.value = i;
                opt.innerHTML = i;
                select.appendChild(opt);
            } 
        }
    }else{
        for (var i = minday; i<=maxday-1; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        } 
    }
    

}

function leapYear(){
    var year = document.getElementById('yearId').value;
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}
